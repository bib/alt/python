def turing(ruban, etape, regles, iterations):
    position = 0
    while iterations:
        # si pas de symbole dans le ruban, ajouter un blanc
        if position >= len(ruban):
            ruban.append(0)
        # lire la valeur du ruban à la position actuelle
        symbole_lu = ruban[position]
        # chercher une regles correspondante
        clef = (etape, symbole_lu)
        if clef in regles:
            # règle trouvée
            valeur = regles[clef]
            nouvelle_etape = valeur[0]
            symbole_ecrit = valeur[1]
            direction = valeur[2]
            # écrire le nouveau symbole de la règle"
            ruban[position] = symbole_ecrit
            # déplacer le ruban dans la direction indiquée"
            if direction:
                position = position + 1
            else:
                position = position - 1
            # enregistrer la nouvelle valeur de l'étape"
            etape = nouvelle_etape
        else:
            # règle non trouvée: arrêter la boucle
            iterations = 1
        iterations = iterations - 1
    return etape

def turing2(ruban, etape, regles, iterations):
    position = 0
    for iteration in range(iterations):
        if position >= len(ruban):                            # ajouter un blanc si nécessaire
            ruban.append(0)
        symbole = ruban[position]                             # lire le symbole
        if (etape, symbole) not in regles:                    # pas de règles: s'arrêter maintenant
            break
        etape, symbole, direction = regles[etape, symbole]    # lire la regle
        ruban[position] = symbole                             # écrire le nouveau symbole
        position += [-1, 1][direction]                        # déplacer le ruban, en changeant la position
    return etape                                              # retourner l'étape finale
